﻿namespace WorkProgram2._0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BookBTN = new System.Windows.Forms.Button();
            this.InProgressBTN = new System.Windows.Forms.Button();
            this.scheduleBTN = new System.Windows.Forms.Button();
            this.AuthorBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BookBTN
            // 
            this.BookBTN.BackColor = System.Drawing.Color.SaddleBrown;
            this.BookBTN.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BookBTN.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BookBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BookBTN.Location = new System.Drawing.Point(-1, -1);
            this.BookBTN.Name = "BookBTN";
            this.BookBTN.Size = new System.Drawing.Size(202, 62);
            this.BookBTN.TabIndex = 0;
            this.BookBTN.Text = "Books";
            this.BookBTN.UseVisualStyleBackColor = false;
            this.BookBTN.Click += new System.EventHandler(this.button1_Click);
            // 
            // InProgressBTN
            // 
            this.InProgressBTN.BackColor = System.Drawing.Color.SaddleBrown;
            this.InProgressBTN.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.InProgressBTN.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.InProgressBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InProgressBTN.Location = new System.Drawing.Point(-1, 61);
            this.InProgressBTN.Name = "InProgressBTN";
            this.InProgressBTN.Size = new System.Drawing.Size(202, 62);
            this.InProgressBTN.TabIndex = 1;
            this.InProgressBTN.Text = "In Progress";
            this.InProgressBTN.UseVisualStyleBackColor = false;
            this.InProgressBTN.Click += new System.EventHandler(this.InProgressBTN_Click);
            // 
            // scheduleBTN
            // 
            this.scheduleBTN.BackColor = System.Drawing.Color.SaddleBrown;
            this.scheduleBTN.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.scheduleBTN.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.scheduleBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scheduleBTN.Location = new System.Drawing.Point(-1, 123);
            this.scheduleBTN.Name = "scheduleBTN";
            this.scheduleBTN.Size = new System.Drawing.Size(202, 62);
            this.scheduleBTN.TabIndex = 2;
            this.scheduleBTN.Text = "Schedule";
            this.scheduleBTN.UseVisualStyleBackColor = false;
            // 
            // AuthorBTN
            // 
            this.AuthorBTN.BackColor = System.Drawing.Color.SaddleBrown;
            this.AuthorBTN.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.AuthorBTN.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.AuthorBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AuthorBTN.Location = new System.Drawing.Point(0, 185);
            this.AuthorBTN.Name = "AuthorBTN";
            this.AuthorBTN.Size = new System.Drawing.Size(202, 62);
            this.AuthorBTN.TabIndex = 3;
            this.AuthorBTN.Text = "Authors";
            this.AuthorBTN.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.AuthorBTN);
            this.Controls.Add(this.scheduleBTN);
            this.Controls.Add(this.InProgressBTN);
            this.Controls.Add(this.BookBTN);
            this.Name = "Form1";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BookBTN;
        private System.Windows.Forms.Button InProgressBTN;
        private System.Windows.Forms.Button scheduleBTN;
        private System.Windows.Forms.Button AuthorBTN;
    }
}

